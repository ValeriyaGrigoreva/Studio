var gulp = require("gulp");
var sass = require("gulp-sass");
var csso = require('gulp-csso');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

gulp.task("css", function () {
    gulp.src("scss/style.scss")
        .pipe(sass())

        /*.pipe(autoprefixer({
        browsers: ['last 16 versions'],
        cascade: false
        }))*/
        .pipe(gulp.dest('dist'));
});

gulp.task("watch", function () {
    gulp.watch("scss/*", ["css"]);

});
gulp.task('autoprefixer', function () {
  var plugins = [
    autoprefixer({browsers: ['last 3 versions','last 2 Edge versions']}),
  ];
  return gulp.src('dist/*.css')
    .pipe(postcss(plugins))
    .pipe(csso())
    .pipe(gulp.dest('dist'));
});


gulp.task("default", ["watch"]);